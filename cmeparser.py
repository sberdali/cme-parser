from bs4 import BeautifulSoup
import requests
import pandas as pd
import datetime
import dataframe_image as dfi
import numpy as np

# fake header to bypass parser locker
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 '
                  'Safari/537.36',
    "Upgrade-Insecure-Requests": "1", "DNT": "1",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate"}

# actual parsing part
html = requests.get("https://www.cmegroup.com/trading/price-limits.html#equityIndex",
                    headers=headers)
soup = BeautifulSoup(html.content, 'lxml')
date_now = datetime.datetime.now()
rows = []
config = pd.read_csv('config.csv')
symbols = config['symbol']
numeric_columns = ['Reference Price',
                   '7% price limit down only',
                   '13% price limit down only',
                   '20% price limit down only',
                   '7%up',
                   '7%down']

for row in soup.select('tbody tr'):
    rows.append([x.text for x in row.find_all('td')])

result = []
for row_list in rows:
    if len(row_list) == 6:
        sym = (row_list[0].split())[-1][1:-1]
        if sym in symbols.values:
            row_list.append(config.loc[config['symbol'] == sym]['type'].values[0])
            result.append(row_list)

df = pd.DataFrame(result, columns=['Contracts',
                                   'Reference Price',
                                   '7%upanddown',
                                   '7% price limit down only',
                                   '13% price limit down only',
                                   '20% price limit down only',
                                   'symbol'
                                   ])

# styling part
# splitting column to two
df[['7%up', '7%down']] = df['7%upanddown'].str.split(' / ', expand=True)

# some transformations according to symbol type
for nc in numeric_columns:
    df[nc] = df[nc].astype('float')
    df[nc] = np.where(df['symbol'] == 'decimal', df[nc] / 100, df[nc])
    df.loc[:, nc] = df[nc].map('{:,.2f}'.format)

# new, colored dataframe
newdf = df[['Contracts',
            'Reference Price',
            '7%up',
            '7%down',
            '7% price limit down only',
            '13% price limit down only',
            '20% price limit down only']]

# using multiindex for blue column header
newdf.columns = pd.MultiIndex.from_tuples([("Contracts", ""),
                                           ("Reference Price", ""),
                                           ("7% price limit (overnight hours) & 5.00 p.m. to 8:30 a.m. up and down",
                                            "UP"),
                                           ("7% price limit (overnight hours) & 5.00 p.m. to 8:30 a.m. up and down",
                                            "DOWN"),
                                           ("7% price limit down only", ""),
                                           ("13% price limit down only", ""),
                                           ("20% price limit down only", "")])


# highlight functions
def highlight_col(x):
    ca = 'background-color: #e2efda'
    cb = 'background-color: #ffcccc'
    df1 = pd.DataFrame('', index=x.index, columns=x.columns)
    df1.iloc[:, 2] = ca
    df1.iloc[:, 3] = cb

    return df1


def highlight_c(s):
    return [
        "background-color: #bdd7ee;" if "7% price limit (overnight hours) & 5.00 p.m. to 8:30 a.m. up and down" in v \
            else "" for v in s]


def highlight_ca(s):
    return ["background-color: #e2efda;" if "UP" in v else "" for v in s]


def highlight_cb(s):
    return ["background-color: #ffcccc;" if "DOWN" in v else "" for v in s]


def highlight_e(s):
    return ["background-color: #fce4d6;" if "7% price limit down only" in v else "" for v in s]


def highlight_f(s):
    return ["background-color: #f8cbad;" if "13% price limit down only" in v else "" for v in s]


def highlight_g(s):
    return ["background-color: #f4b084;" if "20% price limit down only" in v else "" for v in s]


# caption in top of dataframe, +1 day for tomorrow, +3 days if today is Friday
def get_caption(x):
    if x.strftime("%A") == 'Friday':
        second_date = x + datetime.timedelta(3)
    else:
        second_date = x + datetime.timedelta(1)

    return str('For ' + x.strftime("%B") + ' ' + x.strftime("%d") + ', ' + x.strftime(
        "%G") + ' (5pm cst) & ' + second_date.strftime("%B") + ' ' + second_date.strftime(
        "%d") + ', ' + second_date.strftime("%G") + '(RTH - 8:30am - 3.00pm cst) <br> * Source: '
                                                    'https://www.cmegroup.com/trading/price-limits.html#equityIndex '
               )


col_subset = ['Reference Price',
              '7% price limit (overnight hours) & 5.00 p.m. to 8:30 a.m. up and down',
              '7% price limit down only',
              '13% price limit down only',
              '20% price limit down only'
              ]

# applying styles
styled = newdf.style.hide(axis='index') \
    .set_properties(subset=['Contracts'], **{'width': '250px'}) \
    .set_properties(subset=col_subset, **{'width': '75px'}) \
    .set_table_attributes("style='display:inline'") \
    .set_caption(get_caption(date_now)) \
    .apply_index(highlight_c, axis="columns", level=[0]) \
    .apply_index(highlight_ca, axis="columns", level=[1]) \
    .apply_index(highlight_cb, axis="columns", level=[1]) \
    .apply_index(highlight_e, axis="columns", level=[0, 1]) \
    .apply_index(highlight_f, axis="columns", level=[0, 1]) \
    .apply_index(highlight_g, axis="columns", level=[0, 1]) \
    .apply(highlight_col, axis=None) \
    .set_table_styles([{"selector": "tbody td", "props": [("border", "1px solid grey")]},
                       {"selector": "th", "props": [("border", "1px solid grey"), ('text-align', 'center')]}])

dfi.export(styled, 'dataframe.png')
